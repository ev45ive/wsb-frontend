Vue.component('product-details', {
    props: ['product'],
    data: () => ({
        tab: 'home'
    }),
    template:/* html */`<div>
        <div class="row">
            <div class="col-12">  
                <h3 class="">Product Details</h3>
            </div>
            <div class="col">
                <dl>
                    <dt>Product Name:</dt>
                    <dd>{{product.name}}</dd>

                    <dt>Product Price:</dt>
                    <dd>$ {{product.price}}</dd>

                    <dt>Description:</dt>
                    <dd>...</dd>
                </dl>
            </div>
            <add-to-cart :product="product"></add-to-cart>
        </div>
        
        <ul class="nav nav-tabs">
            <li class="nav-item"> <a href="#home" class="nav-link" @click="tab = 'home'" role="tab">About</a> </li>
            <li class="nav-item"> <a class="nav-link" href="#profile" @click="tab = 'profile'" role="tab">Reviews</a> </li>
            <li class="nav-item"> <a href="#contact" class="nav-link" @click="tab = 'contact'" role="tab">Contact</a> </li>
        </ul>
        
        <div class="tab-content my-2" id="myTabContent">
            <div class="tab-pane fade show active" v-if="tab == 'home'" role="tabpanel" aria-labelledby="home-tab">
            Lorem ipsum
            dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore
            magna
            aliqua. Ut enim ad minim veniam.</div>
            <div class="tab-pane fade show active" v-if="tab == 'profile'" role="tabpanel"
            aria-labelledby="profile-tab">Quis nostrud
            exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in
            reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur.</div>
            <div class="tab-pane fade show active" v-if="tab == 'contact'" role="tabpanel"
            aria-labelledby="contact-tab">Excepteur sint
            occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.
            Consectetur adipiscing elit, sed do eiusmod tempor incididunt. </div>
        </div>
    </div>`
})

{/* <div class="d-none d-sm-block col-md-6"></div> */ }
Vue.component('add-to-cart', {
    props: ['product'],
    template:/* html */`<div class="col-md col-md-6">
        <a class="btn btn-secondary btn-block" href="#">Add to Wishlist</a>
        <a class="btn btn-primary btn-block" href="#">Add to Cart</a>
    </div>`
})

Vue.component('products-list', {
    props: ['products', 'selected'],
    template:/* html */`<div>
        <h3 class="">List of products<br></h3>
        <div class="list-group">
            <a href="#" class="list-group-item list-group-item-action" @click="$emit('select',prod)"
            :class="{active: selected && (selected.id == prod.id)}" v-for="prod in products">
            {{prod.name}}
            </a>
        </div>
    </div>`
})

Vue.component('login-form', {
    data() {
        return {
            email: '',
            password: '',
            message: '',
        }
    },
    template:/* html */`<div>
    <h3>Login</h3>
    <form class="">
      <!-- {{email}} {{password}} -->
        <p class="alert alert-danger" v-if="message">{{message}}</p>

        <div class="form-group">
            <input type="email" class="form-control" v-model="email" name="email" placeholder="E-mail">
        </div>
        <div class="form-group">
            <input type="password" class="form-control" v-model="password" name="password" placeholder="Password">
        </div>
        <div class="form-group">
            <button class="btn btn-success" type="button" @click="loginUser()">Login</button>
        </div>
        </form>
    </div>`,
    methods: {
        loginUser() {
            if (!this.email || !this.password) {
                this.message = 'Invalid email or password'
                setTimeout(() => {
                    this.message = ''
                }, 2000)
                return
            }
        }
    }
})


var app = new Vue({
    el: '#app',
    data: {
        user: {
            name: 'Guest'
        },
        products: [
            { id: '123', name: 'Product 123', price: 100 },
            { id: '345', name: 'Product 345', price: 200 },
            { id: '456', name: 'Product 456', price: 110 },
            { id: '567', name: 'Product 567', price: 140 },
        ],
        product: null,
    },
    methods: {
        selectProduct(product) {
            this.product = product
        }
    }
})
