https://nodejs.org/en/

## Check node version
node -v
v14.17.0

## Restart VsCode
https://content.spiceworksstatic.com/service.community/p/post_images/0000120691/560e8fd5/attached_image/dt990804dhc0.gif


## Make directory, print to file
mkdir backend
echo 'hello' > backend/index.js

## Start program
node backend/index.js