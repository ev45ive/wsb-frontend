https://git-scm.com/download/win
https://sourceforge.net/projects/git-osx-installer/files/latest/download
https://www.atlassian.com/git/tutorials/install-git

## Lokalne repozytorium
git init .

## Stworzenie migawki / zestawu zmian (commit)
git add .
git commit -m "MOje zmiany"

## Wypychamy zmiany na zdalne repozytorium
git remote add origin git@bitbucket.org:ev45ive/wsb-frontend.git
git push -u origin master


## Pobieramy zmiany ze zdalnego repozytorium
git clone https://bitbucket.org/ev45ive/wsb-frontend.git wsb-frontend
cd wsb-frontend

git pull

## Rozszerzenia
https://marketplace.visualstudio.com/items?itemName=eamodio.gitlens


## NPM
npm install nazwa-paczki
npm i express

git pull

cd backend
npm i 
npm start