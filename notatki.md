## Rozwiazanie nazwy

https://pl.wikipedia.org/wiki/Domain_Name_System

https://pl.wikipedia.org/wiki/Adres_IP
https://pl.wikipedia.org/wiki/Port_protoko%C5%82u

## Zapytanie

https://pl.wikipedia.org/wiki/Hypertext_Transfer_Protocol

GET http://google.com/?query=wsb HTTP/1.1
host: http://google.com/
User-Agent: Mozilla/5.0 (X11; U; Linux i686; pl; rv:1.8.1.7) Gecko/20070914 Firefox/2.0.0.7

## Odpowiedz
https://pl.wikipedia.org/wiki/Kod_odpowiedzi_HTTP
https://pl.wikipedia.org/wiki/Lista_nag%C5%82%C3%B3wk%C3%B3w_HTTP

## API
Kontrakt 
https://pl.wikipedia.org/wiki/Interfejs_programowania_aplikacji

https://pl.wikipedia.org/wiki/HTML
https://pl.wikipedia.org/wiki/JSON


## REST API
https://en.wikipedia.org/wiki/Representational_state_transfer
https://www.kennethlange.com/dont-limit-your-rest-api-to-crud-operations/

